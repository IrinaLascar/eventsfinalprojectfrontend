import { Component } from '@angular/core';
import { Event } from 'src/app/model/event';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from "@angular/router";

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent {
  event: Event = new Event(null,
    null,
    null,
    null,
    null,
    null,
    null);

  httpClient: HttpClient;
  route: ActivatedRoute

  constructor(httpClient: HttpClient, route: ActivatedRoute, private router: Router) {//daca declar un field privat in constructor, nu mai tb sa-l declar si nici sa asignez
    this.httpClient = httpClient;
    this.route = route;
  }

  ngOnInit(): void {
    const eventId = this.route.snapshot.paramMap.get("id"); // ptr a accesa parametrul din url
    //accesam evenimentul cu id-ul 1 - response e obiectul ce-l primim
    this.httpClient.get('/api/events/' + eventId).subscribe((response) => {
        console.log(response);
        this.event = response as Event;
        if (this.event.startDate != null)
          this.event.startDate = new Date(this.event.startDate!);
        if (this.event.endDate != null)
          this.event.endDate = new Date(this.event.endDate!);
      },

      //.subscribe() are 2 parametri: () => {} - daca totul e ok si () = > {} daca avem o eroare
      (error) => {
        console.log(error);
        if(error.error == "There is no event with ID:" + eventId)
        this.router.navigate(["/not-found"])

      });
  }
}

